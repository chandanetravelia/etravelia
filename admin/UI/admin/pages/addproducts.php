
<section class="content-header">
    <h1>
        Packages 
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add product</li>
    </ol>
</section>
<br>
<form action="" method="POST" enctype="multipart/form-data">
    <!-- Main content put content inside it-->
    <section class="content" id="sectiondiv">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add Package</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div  style="display: none; margin-left: 10px;margin-right: 10px; margin-top: 10px;" id="showErr" class="alert alert-danger" ></div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-md-12">
                                <form method="post" action="">

                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Package Name</label>
                                            <input type="text" name="pkg_name"title="The Beautiful Package Name" id="pkg_name" value="" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Package Type</label>
                                            <select class="form-control" id="pkg_type" title="Enter Package Type">
                                                <option>Eco</option>
                                                <option>Delux</option>

                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Category Name</label>
                                            <select class="form-control" id="pkg_category">
                                                <option disabled>Category Name</option>
                                                <option selected>Adventure</option>
                                                <option>Family</option>
                                                

                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Activities Included</label>
                                            <select class="form-control" id="pkg_activities">
                                                <option >High Jump</option>
                                                <option >Tracking</option>
                                                <option >Rafting</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Package Days</label>
                                            <input type="number" onchange="addDaysDescription()" name="pkg_days"title="No of Days" id="pkg_days" value="" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Package Nights</label>
                                            <input type="text" name="pkg_nights"title="No of Nights" id="pkg_nights" value="" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Best Month To Visit</label>
                                            <select class="form-control" id="pkg_month">
                                                <option >Jan</option>
                                                <option >Feb</option>
                                                <option >Mar</option>
                                                <option >April</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">Uploads Images</label><br>
                                        <button type="button" id="" title="Enter addition Information(Optional)" class="btn btn-success" data-toggle="modal" data-target="#ImageModal" >Upload Images</button>

                                    </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.box -->
                </div>

                <div class="box">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Package Iteration Details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div  style="display: none; margin-left: 10px;margin-right: 10px; margin-top: 10px;" id="showErr" class="alert alert-danger" ></div>
                        <div class="row" style="margin-top: 10px;">
                            <div class="col-md-12">
                                <div class="col-md-10" id="daysbuttondiv">
                                    <!--see js-->
                                </div>
                                <div class="col-md-2">
                                    <input type="button" id="add_packages"class="btn btn-success" value="Add Package">
                                    <input type="reset" id="reset_form"class="hidden">
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- /.box -->
                </div>



            </div>
        </div>

        <!--  Upload Images Division start here    -->
        <div class="modal fade" id="ImageModal" tabindex="-1" role="dialog" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Uploads Package Images</h4>
                        <a onclick="insertNewRow()" class="btn btn-warning">Add New Image</a>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-6" id="pkgImagesDiv">
                                <!--see js-->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="" class="btn btn-default" data-dismiss="modal">Done</button>
                        <!-- <button type="button" class="btn btn-primary" id="">Done</button>-->
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="daysModal" tabindex="-1" role="dialog" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Day 1 Description</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Place Name</label>
                                    <input type="text" name="place_name"  id="place_name" value="" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group label-floating">
                                    <label class="control-label">Images</label>
                                    <input type="file" name="image" id="image" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="checkbox">
                                    <label> <input type = "checkbox" id ="" value ="">BreakFast</label> 
                                </div>
                                <div class="checkbox">
                                    <label> <input type = "checkbox" id ="" value ="">Lunch</label> 
                                </div>
                                <div class="checkbox">
                                    <label> <input type = "checkbox" id ="" value ="">Dinner</label> 
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group label-floating">
                                    <label class="control-label">Hotel Name</label>
                                    <input type="text" name="hotel_name" id="hotel_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Description</label>
                                    <input type="textarea" name="desc" id="desc" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="" class="btn btn-default" data-dismiss="modal">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

</form>


<script src="./dist/js/pages/packages.js" type="text/javascript"></script>
