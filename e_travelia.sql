-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2018 at 09:47 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_travelia`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) UNSIGNED NOT NULL,
  `admin_name` varchar(50) NOT NULL,
  `admin_phone` bigint(20) UNSIGNED NOT NULL COMMENT 'length can vary between 6-15',
  `admin_email` varchar(50) DEFAULT NULL,
  `company_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `admin_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1-superadmin',
  `admin_rights` varchar(500) NOT NULL COMMENT '# seperated format',
  `username` varchar(512) NOT NULL,
  `password` varchar(512) NOT NULL,
  `addedby` int(11) NOT NULL DEFAULT '0' COMMENT 'super admin id , default 0',
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_name`, `admin_phone`, `admin_email`, `company_id`, `admin_type`, `admin_rights`, `username`, `password`, `addedby`, `timestamp`) VALUES
(1, 'Username', 8826853863, 'nishantbhushan86@gmail.com', 1, 1, '1#2#3#4#5#6#7#8#9#10', 'username', '16F78A7D6317F102BBD95FC9A4F3FF2E3249287690B8BDAD6B7810F82B34ACE3', 0, '2018-02-15 12:39:16'),
(12, 'chandan', 8743870503, 'daredarkshadow@gmail.com', 1, 1, '1#2#3#4#5#6#7#8#9#10', 'username1', '9671f8128b2f7094c2444768179788f3407169683a33d8bcb5db7250fbdc36d8', 0, '2018-02-15 12:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email` varchar(50) DEFAULT '',
  `service_name` varchar(100) NOT NULL,
  `details` varchar(500) DEFAULT '',
  `file_url` varchar(50) DEFAULT '',
  `pref_date` date NOT NULL,
  `pref_time` time DEFAULT NULL,
  `is_confirmed` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `appointment_schedule`
--

CREATE TABLE `appointment_schedule` (
  `day` varchar(10) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `duration_in_min` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment_schedule`
--

INSERT INTO `appointment_schedule` (`day`, `start_time`, `end_time`, `duration_in_min`) VALUES
('FRIDAY', '11:00:00', '17:00:00', 30),
('MONDAY', '10:00:00', '17:00:00', 45),
('SATURDAY', '10:00:00', '17:00:00', 30),
('SUNDAY', '11:00:00', '14:00:00', 30),
('THURSDAY', '09:00:00', '17:00:00', 40),
('TUESDAY', '10:00:00', '16:00:00', 30),
('WEDNESDAY', '10:00:00', '18:00:00', 30);

-- --------------------------------------------------------

--
-- Table structure for table `associate_partner`
--

CREATE TABLE `associate_partner` (
  `assoc_id` int(11) UNSIGNED NOT NULL,
  `ref_user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `mobile` bigint(15) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL DEFAULT 'not defined',
  `is_company` tinyint(3) NOT NULL DEFAULT '0',
  `ref_company_id` tinyint(3) NOT NULL DEFAULT '0',
  `document_no` varchar(50) NOT NULL DEFAULT '0' COMMENT '# seperated parallel to document',
  `document_name` varchar(500) NOT NULL DEFAULT '0' COMMENT '# seperated parallel to identification',
  `document_url` varchar(500) NOT NULL DEFAULT '0' COMMENT '# seperated',
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `address` varchar(500) NOT NULL DEFAULT 'NULL',
  `gender` char(1) NOT NULL,
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `associate_partner`
--

INSERT INTO `associate_partner` (`assoc_id`, `ref_user_id`, `name`, `mobile`, `email`, `is_company`, `ref_company_id`, `document_no`, `document_name`, `document_url`, `status`, `address`, `gender`, `dob`, `added_by`) VALUES
(1, 117, 'salman khan bhai', 3838383838, 'admin@123.com', 0, 0, '12 # 132', 'qw # 234', '3008.jpg # 4157.jpg', 0, 'greate noida', 'f', '1212-12-12', 1),
(4, 122, 'admin name', 3838383838, 'admin@123.com', 0, 22, '12`', 'chandan', '3008.jpg', 0, 'greate noida', 'm', '1212-12-12', 1),
(9, 134, 'test', 1232323456, 'chandan@gmail.com', 0, 0, '12112', 'qwqwqw', '3170.jpg', 1, 'ssdxcwwefdcffvr', 'm', '2018-08-21', 1),
(26, 153, 'Abhishek', 2234495494, 'chandan@gmail.com', 1, 26, 'dfkfdk444i3k # dfkfdk444i3k442', 'hello do # hello doqqq', 'PuttyBeach_EN-IN8858831610_1366x768.jpg # ac.JPG', 1, 'Greate Noida', 'm', '2018-08-21', 1),
(27, 158, 'new associate', 2232334343, 'chandan@gmail.com', 1, 27, '12qqws2 # 13', 'qwfddd # qwe', '741.jpg # 858.jpg', 1, 'greater Noida', 'm', '2018-08-21', 1),
(28, 159, 'chandan', 1123323434, 'chandan@GMAM.com', 1, 28, 'jkjkjlksdj', 'kjkjl;kj;lkj', '', 1, 'aaasass', 'M', '2018-10-30', 1),
(29, 161, 'chandan', 1123323434, 'chandan@GMAM.com', 1, 29, 'jkj # kjlksdj', 'kjkj # l;kj;lkj', '3008.jpg # 3170.jpg', 1, 'aaasass', 'M', '2018-10-30', 1),
(30, 162, 'Hello associate', 3433439434, 'hello@gmail.com', 0, 0, 'doc1', 'docname1', '1470.jpg', 1, 'Nichlaul', 'M', '2018-05-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED DEFAULT NULL COMMENT '0 if category is parent',
  `category_name` varchar(50) NOT NULL,
  `addedby` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `parent_id`, `category_name`, `addedby`, `timestamp`, `status`) VALUES
(44, 0, 'Family Pkg', 1, '2018-03-12 14:05:55', 1),
(45, 0, 'Accesseries', 1, '2018-03-12 14:06:13', 1),
(46, 45, 'HoneyMoon ', 1, '2018-03-12 22:55:45', 1),
(47, 0, 'Medicines', 1, '2018-03-14 12:33:10', 1),
(48, 0, 'Hello there', 12, '2018-04-14 14:29:23', 1),
(49, 0, 'Hello world', 12, '2018-04-14 14:34:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) UNSIGNED NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_logo_url` varchar(100) DEFAULT NULL,
  `company_address` varchar(150) DEFAULT NULL,
  `company_phone` bigint(20) UNSIGNED NOT NULL,
  `company_state` varchar(50) DEFAULT NULL,
  `company_country` varchar(50) DEFAULT NULL,
  `gstin` varchar(20) DEFAULT NULL,
  `tin_no` bigint(20) UNSIGNED DEFAULT NULL,
  `pan_no` varchar(15) DEFAULT NULL,
  `added_by` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_logo_url`, `company_address`, `company_phone`, `company_state`, `company_country`, `gstin`, `tin_no`, `pan_no`, `added_by`, `timestamp`) VALUES
(1, 'bsrnetwork', 'abc.jpg', 'Greate noida', 323234543, 'Kerla', 'India', 'KJK43544KL', 234345454, 'CXCDE324334', 1, '2018-03-23 17:44:41'),
(22, 'Bs', 'shoks.jpeg', 'Greate noida', 323234543, 'Kerla', 'India', 'KJK43544KL', 234345454, 'CXCDE324334', 1, '2018-04-06 13:39:35'),
(25, 'Bs', '', 'Greate noida', 323234543, 'Kerla', 'India', '234345454', 0, 'CXCDE324334', 1, '2018-04-10 17:53:38'),
(26, 'hello company', '', 'Greater NOida', 4848484848484, 'up', 'India', '1112394959', 0, 'FFR84ddr', 1, '2018-04-12 21:17:41'),
(27, 'new company 1', '', 'greate noida', 11112123454, 'mp', 'india', '112344583423', 184467440737095, 'ssqwessd', 1, '2018-04-13 20:08:47'),
(28, 'new comany fds;flksdjfs;dlfkj', '', ';lkjl', 0, ';kj', ';lkj', ';lkj', 0, ';lkj', 1, '2018-04-16 17:50:11'),
(29, 'new comany fds;flksdjfs;dlfkj', '', ';lkjl', 0, ';kj', ';lkj', ';lkj', 0, ';lkj', 1, '2018-04-16 17:54:46');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customer_id` int(11) UNSIGNED NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_email` varchar(50) DEFAULT '',
  `customer_phone` bigint(20) DEFAULT '0' COMMENT 'can vary from 6-15',
  `customer_address` varchar(150) DEFAULT '',
  `is_a_company` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `company_reg_id` int(11) NOT NULL DEFAULT '0',
  `addedby` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_add` varchar(150) NOT NULL,
  `customer_state` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `customer_name`, `customer_email`, `customer_phone`, `customer_address`, `is_a_company`, `company_id`, `company_reg_id`, `addedby`, `timestamp`, `customer_add`, `customer_state`) VALUES
(143, 'rahul', 'rahul@gmail.com', 1212121212, 'maharajganj up.', 0, 2, 2, 1, '2018-02-15 12:56:12', '', 0),
(144, 'Gaurav Thakur particu', 'gaurav@gmail.com', 9077657768, 'shimala near mount everest', 0, 0, 0, 1, '2018-02-15 22:30:41', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_no` int(11) NOT NULL,
  `selling_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total_amount_without_tax` float NOT NULL,
  `total_tax` float NOT NULL,
  `other_charges` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `other_charges_description` varchar(150) NOT NULL,
  `invoice_total` float NOT NULL,
  `paid_amount` float NOT NULL,
  `due_amount` float DEFAULT '0',
  `due_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `payment_method` varchar(20) DEFAULT NULL,
  `transaction_number` varchar(100) DEFAULT NULL,
  `tranNoOrCashPayed` varchar(100) DEFAULT NULL COMMENT 'Cash payed if cash, else Txn Number , cheque number',
  `addedby` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_no`, `selling_id`, `invoice_date`, `customer_id`, `total_amount_without_tax`, `total_tax`, `other_charges`, `other_charges_description`, `invoice_total`, `paid_amount`, `due_amount`, `due_date`, `payment_method`, `transaction_number`, `tranNoOrCashPayed`, `addedby`, `timestamp`) VALUES
(147, 148, '2018-02-15', 143, 40195, 0, 0, '*It may include shipping and other charges.', 40195, 40195, 0, '2018-02-15 10:24:42', NULL, NULL, NULL, 1, '2018-02-15 10:24:42'),
(148, 149, '2018-02-15', 144, 529, 119, 0, '*It may include shipping and other charges.', 649, 649, 0, '2018-02-15 17:00:41', NULL, NULL, NULL, 1, '2018-02-15 17:00:41'),
(149, 150, '2018-02-16', 143, 70, 12, 50, '*It may include shipping and other charges.', 133, 100, 33, '2018-02-16 05:22:55', NULL, NULL, NULL, 1, '2018-02-16 05:22:56');

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `pkg_id` int(11) NOT NULL,
  `pkg_name` varchar(256) NOT NULL,
  `pkg_type` varchar(100) DEFAULT NULL COMMENT 'standerds,delux # seperated',
  `pkg_category` varchar(100) DEFAULT NULL COMMENT 'Advanture,tour,group # seperated',
  `pkg_activities` varchar(200) DEFAULT NULL COMMENT '# seperated',
  `pkg_days` int(3) NOT NULL,
  `pkg_month` varchar(100) NOT NULL,
  `pkg_price` float NOT NULL,
  `pkg_images` varchar(500) NOT NULL,
  `places_name` varchar(1000) NOT NULL,
  `places_desc` varchar(1000) NOT NULL COMMENT '# seperated',
  `places_image` varchar(1000) DEFAULT NULL COMMENT '# seperated',
  `places_meal` varchar(500) DEFAULT NULL COMMENT '# seperated',
  `places_hotel` varchar(500) DEFAULT NULL COMMENT '# seperated',
  `addedby` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`pkg_id`, `pkg_name`, `pkg_type`, `pkg_category`, `pkg_activities`, `pkg_days`, `pkg_month`, `pkg_price`, `pkg_images`, `places_name`, `places_desc`, `places_image`, `places_meal`, `places_hotel`, `addedby`, `is_active`, `status`, `timestamp`) VALUES
(1, 'kullu', 'family', 'delux', 'ehlo()fedf();dfk()', 2, 'jan', 0, '858.jpg#3170.jpg', 'one place#two place', 'desc1#desc2', 'no_image_.jpg#858.jpg', 'khana1#khana2', 'hotel121#hotel2', 1, 1, 0, '2018-04-17 03:04:57'),
(2, 'kullu', 'family', 'delux', 'ehlo()fedf();dfk()', 2, 'jan', 0, '858.jpg#3170.jpg', 'one place#two place', 'desc1#desc2', 'no_image_.jpg#858.jpg', 'khana1#khana2', 'hotel121#hotel2', 1, 1, 0, '2018-04-17 03:05:58'),
(3, 'kullu', 'family', 'delux', 'ehlo()fedf();dfk()', 2, 'jan', 0, '858.jpg#3170.jpg', 'one place#two place', 'desc1#desc2', 'no_image_.jpg#858.jpg', 'khana1#khana2', 'hotel121#hotel2', 1, 1, 0, '2018-04-17 03:07:22'),
(4, ';klj;k', 'Eco', 'Adventure', 'High Jump', 1, 'Jan', 0, '', ';klj', ';kj;lk', 'no_image_.jpg', 'BreakFast', ';lkj;lk', 12, 1, 0, '2018-04-20 02:14:12'),
(5, ';klj;k', 'Eco', 'Adventure', 'High Jump', 1, 'Jan', 0, '', ';klj', ';kj;lk', 'no_image_.jpg', 'BreakFast', ';lkj;lk', 12, 0, 0, '2018-04-20 02:14:26'),
(6, 'Hello pkg', 'Eco', 'Family', 'Tracking', 5, 'Mar', 0, '', 'defd', 'ffdf;jdfsdf;kjsd;kds;kjf', 'no_image_.jpg#no_image_.jpg#no_image_.jpg#no_image_.jpg#no_image_.jpg', 'BreakFast#BreakFast#BreakFast#BreakFast#BreakFast', 'Nifsdfj', 1, 0, 0, '2018-05-12 13:45:04'),
(7, 'new package', 'Delux', 'Family', 'Tracking', 3, 'April', 0, '', 'one#two#three', 'dsklfjsdf;ksdjfsd;kl#;klj;kj;lkj;lkj#kkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 'no_image_.jpg#no_image_.jpg#no_image_.jpg', 'BreakFast#BreakFast#BreakFast', 'New york#;lkj;kj#;lkj;kjkkkkkkkkkkkkkkk', 1, 0, 0, '2018-05-30 01:50:23');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_make` varchar(50) DEFAULT NULL COMMENT 'Brand Name',
  `category_id` int(11) DEFAULT NULL,
  `barcode` varchar(200) DEFAULT NULL,
  `purchase_price` float DEFAULT '0',
  `selling_price` float DEFAULT '0',
  `display_price` tinyint(1) DEFAULT '0',
  `unit_of_measurement` varchar(20) DEFAULT NULL,
  `discount` float DEFAULT '0',
  `product_description` varchar(1000) NOT NULL COMMENT 'html formatted description',
  `hsn` varchar(100) DEFAULT '',
  `product_image_url` varchar(1000) DEFAULT NULL COMMENT 'space#space seperated url of all images',
  `features` varchar(300) NOT NULL COMMENT '# seperated',
  `feature_description` varchar(5000) NOT NULL COMMENT '# sepearted',
  `addedby` int(11) NOT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_make`, `category_id`, `barcode`, `purchase_price`, `selling_price`, `display_price`, `unit_of_measurement`, `discount`, `product_description`, `hsn`, `product_image_url`, `features`, `feature_description`, `addedby`, `timestamp`) VALUES
(11, 'ABC Pkg', 'Pet sof', 44, '', 0, 200, 1, '200 g', 0, 'Pedigree Puppy Chicken &amp; Milk Dog Food is a balanced food for your beloved dog. It s packed with essential nutrients, to build puppy s immune system. On a regular feeding, you can see 5 signs of good health in your dog: healthy skin &amp; coat, healthy teeth &amp; bones, better digestion, strong muscles and better immunity.', '', '197367.jpg # food3.jpg', 'Miniature Pom', '125-160', 1, '2018-03-14 11:49:07'),
(12, 'ak pkg', 'StayCare', 45, '', 0, 200, 0, 'Piece', 0, 'Cereals and cereal by-products; meat and meat by-products; chicken and chicken by-products; vegetables and vegetable by-products; vegetable oils; milk powder; iodised salt; essential vitamins and minerals; permitted preservatives, antioxidants and flavours. No beef or pork.', '', 'DSC_8785_list.JPG # DSC_8856_list.JPG', 'Piecs', '2', 12, '2018-03-14 11:54:11'),
(13, 'Spa material', 'Lakme', 45, '', 0, 0, 0, 'KG', 0, 'My awesome spa for the puppy.', '', 'DSC_8856_list.JPG # project5.png', '#', '#', 1, '2018-03-14 12:15:50'),
(14, 'Ring', 'fkldasjflk', 45, '', 0, 0, 0, 'fdjal', 0, 'fkldjasklfkldsjflks;djaf', '', 'avatar2.png # avatar2.png # back2.jpg', '#', '#', 1, '2018-03-14 12:30:52'),
(15, 'food', 'none', 44, '', 0, 0, 0, 'Kg', 0, 'hdeuidh diewbduiwe', '', 'avatar4.png # avatar1.png', '', '', 1, '2018-03-14 17:41:24'),
(16, 'pkg', 'none', 44, '', 0, 0, 0, 'Kg', 0, 'gdeiwdgiewug iuiwueyiuyw9 9y9yw9 re', '', 'food3.jpg # DSC_8863_medium.JPG', '', '', 1, '2018-03-14 18:00:00'),
(17, 'pkg2', 'None', 44, '', 200, 202, 0, '200 gram', 0, '&lt;p&gt;Dog food is food specifically formulated and intended for consumption by dogs and other related canines. Like all carnivores, dogs have sharp, pointed teeth, and have short gastrointestinal tracts better suited for the consumption of meat.\r\nIn the United States alone, dog owners spent over $8.6 billion on commercially manufactured dog food in 2007.[1] Some people make their own dog food, feed their dogs meals made from ingredients purchased in grocery or health food stores or give their dogs a raw food diet.&lt;/p&gt;', '', 'food.png # food1.jpg # DSC_8863_medium.JPG', 'Item Weight#Item part number', '200gm#7878798', 1, '2018-03-15 14:44:20'),
(18, 'pkg hi', 'Care', 44, '', 190, 200, 0, '200 gm', 0, 'Endi Chews Dental Flavor For Fresh Breath  Strength Bones  Balance Nutrition.Reward treats for all dogs.Good for puppies and old dogs.Meat Up adult dog food is formulated to deliver a complete and balanced, energy packed diet that helps enhance physical performance and supports enduring pet health. It contains all the essential ingredients to satisfy the nutritional needs of your dog. It promotes healthy digestion and provides overall wellness to your dog.', '', 'DSC_8863_medium.JPG # food3.jpg', 'Item Weight#Protein#Ingredient', '200gm#4 cal#meat,rice,egg', 1, '2018-03-15 16:12:00');

-- --------------------------------------------------------

--
-- Table structure for table `selling`
--

CREATE TABLE `selling` (
  `selling_id` int(11) NOT NULL,
  `product_id` varchar(5000) NOT NULL COMMENT 'comma space seperated',
  `product_quantity` varchar(5000) NOT NULL COMMENT 'comma space seperated',
  `unit_of_measurement` varchar(5000) NOT NULL COMMENT 'comma space seperated',
  `unit_rate` varchar(5000) NOT NULL COMMENT 'comma space seperated',
  `discount` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `cost_without_tax` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `cgst` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `sgst` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `igst` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `cess` varchar(5000) DEFAULT NULL COMMENT 'comma space seperated',
  `total_product_amount` varchar(5000) NOT NULL COMMENT 'comma space seperated',
  `addedby` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selling`
--

INSERT INTO `selling` (`selling_id`, `product_id`, `product_quantity`, `unit_of_measurement`, `unit_rate`, `discount`, `cost_without_tax`, `cgst`, `sgst`, `igst`, `cess`, `total_product_amount`, `addedby`, `timestamp`) VALUES
(148, '42#44', '96#2', 'kg#piece', '75#17000', '5#1000', '7195#33000', '0#0', '0#0', '0#0', '0#0', '7195.00#33000.00', 1, '2018-02-15 15:54:42'),
(149, '42#43', '12#1', 'kg#kg', '40#55', '1#11', '479#50', '12#0', '12#0', '0#12', '1#0', '598.75#50', 1, '2018-02-15 22:30:41'),
(150, '42', '1', 'kg', '75', '5', '70', '8', '8', '0', '2', '82.60', 1, '2018-02-16 10:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `service_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `service_name` varchar(50) NOT NULL,
  `punchline` varchar(100) DEFAULT '',
  `thumbnail_url` varchar(100) DEFAULT '',
  `added_by` int(10) UNSIGNED NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`service_id`, `parent_id`, `service_name`, `punchline`, `thumbnail_url`, `added_by`, `timestamp`, `status`) VALUES
(15, 0, 'Dog Breeding', '', '', 1, '2018-03-11 12:07:36', 1),
(18, 0, 'Dog Grooming', '', '', 1, '2018-03-11 13:11:56', 1),
(19, 0, 'Treatment', '', '', 1, '2018-03-11 13:13:53', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_description`
--

CREATE TABLE `service_description` (
  `service_id` int(11) NOT NULL,
  `service_description` varchar(10000) NOT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `stock_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` float NOT NULL DEFAULT '0',
  `unit_of_measurement` varchar(20) NOT NULL,
  `min_quantity` float NOT NULL DEFAULT '0',
  `max_quantity` float NOT NULL DEFAULT '9999',
  `notify` tinyint(1) NOT NULL DEFAULT '1',
  `addedby` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`stock_id`, `product_id`, `quantity`, `unit_of_measurement`, `min_quantity`, `max_quantity`, `notify`, `addedby`, `timestamp`) VALUES
(12, 2, 68768, 'piece', 13, 999, 1, 1, '2018-02-15 12:43:31'),
(13, 42, 858, 'kg', 0, 9999, 1, 1, '2018-02-15 15:48:45'),
(14, 44, 18, 'piece', 0, 9999, 1, 1, '2018-02-15 15:48:53'),
(15, 45, 8, 'piece', 0, 9999, 1, 1, '2018-02-15 15:49:16'),
(16, 48, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:27'),
(17, 49, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(18, 50, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(19, 51, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(20, 52, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(21, 53, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(22, 54, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(23, 55, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(24, 56, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(25, 57, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(26, 58, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(27, 59, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(28, 60, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(29, 61, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(30, 62, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(31, 63, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(32, 64, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(33, 65, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(34, 66, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(35, 67, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(36, 68, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(37, 69, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(38, 70, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(39, 71, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(40, 72, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(41, 73, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(42, 74, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(43, 75, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(44, 76, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(45, 77, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(46, 78, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(47, 79, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(48, 80, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(49, 81, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(50, 82, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(51, 83, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(52, 84, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(53, 85, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(54, 86, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(55, 87, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(56, 88, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(57, 89, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(58, 90, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(59, 91, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(60, 92, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(61, 93, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:28'),
(62, 94, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(63, 95, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(64, 96, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(65, 97, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(66, 98, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(75, 99, 0, '', 0, 9999, 1, 0, '2018-02-16 15:25:29'),
(376, 100, 16008, '', 0, 9999, 1, 0, '2018-02-16 15:36:43'),
(377, 101, 1204, '', 0, 9999, 1, 0, '2018-02-16 15:36:43'),
(378, 102, 1208, '', 0, 9999, 1, 0, '2018-02-16 15:36:43'),
(391, 103, 0, '', 0, 9999, 1, 0, '2018-02-16 15:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `user_name` varchar(512) NOT NULL,
  `user_password` varchar(512) NOT NULL,
  `user_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '1 for SuperAdmin',
  `user_right` varchar(200) DEFAULT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT 'active and deactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_id`, `user_name`, `user_password`, `user_type`, `user_right`, `status`) VALUES
(1, 'admin', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', 1, '1#2#3#4#5#6#7#8#9', 1),
(2, 'employee', '2fdc0177057d3a5c6c2c0821e01f4fa8d90f9a3bb7afd82b0db526af98d68de8', 2, '1#2#3', 1),
(3, 'associate', '178b6e061a04a75e46b85f8c9ef1803583f5cb5a459d4ba45c3b7529c5f0c3ec', 3, '1#2', 1),
(4, 'client', '948fe603f61dc036b5c596dc09fe3ce3f3d30dc90f024c85f3c82db2ccab679d', 4, '1', 1),
(117, 'salluumiya', '179484689d569b692277889cb5bc6c3bc90607af240027d34c4fb30e4436d8e1', 3, '5#6', 1),
(134, 'akash kumar', '38cf0ccfe634fe624044940099f0bf41a77d950dac0f381279c57ac36e375dcf', 3, '1#7#9', 1),
(155, 'pratik mehta', '96217a78979c30c72b7f1b3b36a1e4680293a5ced020e15cebebb5a90d715686', 2, '1#5#6#9', 1),
(156, 'nernerkkkkk', 'e064f4044b700a12ab675480701a95f0060148123d4dad5452c4287c56df410e', 2, '1#5#6#7#9', 1),
(158, 'newuserupdated', '9c9064c59f1ffa2e174ee754d2979be80dd30db552ec03e7e327e9b1a4bd594e', 3, '1#2#7#9', 1),
(159, 'chandan', '9671f8128b2f7094c2444768179788f3407169683a33d8bcb5db7250fbdc36d8', 3, '1#2#3#5#6', 1),
(161, 'chandankj;klj;kj', '9671f8128b2f7094c2444768179788f3407169683a33d8bcb5db7250fbdc36d8', 3, '1#2#3#5#6', 1),
(162, 'Hello associate', '9671f8128b2f7094c2444768179788f3407169683a33d8bcb5db7250fbdc36d8', 3, '1#3#4#5', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_rights`
--

CREATE TABLE `user_rights` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `page_name` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `Display_name` varchar(50) NOT NULL,
  `extra_icon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_rights`
--

INSERT INTO `user_rights` (`id`, `parent_id`, `page_name`, `icon`, `Display_name`, `extra_icon`) VALUES
(1, 0, 'addproducts', 'fa fa-edit', 'Add Package', ''),
(2, 0, 'viewproducts', 'fa fa-eye', 'View/Edit Package', ''),
(3, 0, 'addcategory', '	fa fa-clone', 'Category', ''),
(4, 0, 'service', 'fa fa-edit', 'Services', ''),
(5, 0, 'dashboard', 'fa fa-dashboard', 'Dashboard', ''),
(6, 0, 'addAssociate', 'fa fa-dashboard', 'Add Associate', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `appointment_schedule`
--
ALTER TABLE `appointment_schedule`
  ADD PRIMARY KEY (`day`);

--
-- Indexes for table `associate_partner`
--
ALTER TABLE `associate_partner`
  ADD PRIMARY KEY (`assoc_id`),
  ADD UNIQUE KEY `ref_user_id` (`ref_user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD UNIQUE KEY `parent_id` (`parent_id`,`category_name`),
  ADD UNIQUE KEY `parent_id_2` (`parent_id`,`category_name`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `customer_name` (`customer_name`,`customer_email`,`customer_phone`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_no`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`pkg_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `product_name` (`product_name`,`product_make`,`category_id`),
  ADD UNIQUE KEY `product_name_2` (`product_name`,`product_make`,`category_id`);

--
-- Indexes for table `selling`
--
ALTER TABLE `selling`
  ADD PRIMARY KEY (`selling_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`),
  ADD UNIQUE KEY `parent_id` (`parent_id`,`service_name`);

--
-- Indexes for table `service_description`
--
ALTER TABLE `service_description`
  ADD UNIQUE KEY `service_id` (`service_id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`stock_id`),
  ADD UNIQUE KEY `product_id` (`product_id`),
  ADD UNIQUE KEY `product_id_2` (`product_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `user_rights`
--
ALTER TABLE `user_rights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `associate_partner`
--
ALTER TABLE `associate_partner`
  MODIFY `assoc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customer_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `pkg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `selling`
--
ALTER TABLE `selling`
  MODIFY `selling_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT for table `user_rights`
--
ALTER TABLE `user_rights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
