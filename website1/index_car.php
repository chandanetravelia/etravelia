<!--HEADER SECTION-->
	<section>
		<div class="v2-hom-search">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
					<div class="">
						<form class="v2-search-form">
							<div class="row">
								<div class="input-field col s12">
									<input type="text" id="select-city" class="autocomplete">
									<label for="select-city">Pick up location</label>
								</div>
								<div class="input-field col s12">
									<input type="text" id="select-city-1" class="autocomplete">
									<label for="select-city">Dropping off location</label>
								</div>								
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="from" name="from">
									<label for="from">Pick up date</label>
								</div>
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Pick up time</option>
										<option value="1">12:00 AM</option>
										<option value="1">01:00 AM</option>
										<option value="1">02:00 AM</option>
										<option value="1">03:00 AM</option>
										<option value="1">04:00 AM</option>
										<option value="1">05:00 AM</option>
										<option value="1">06:00 AM</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" id="to" name="to">
									<label for="to">Drop off date</label>
								</div>
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Drop off time</option>
										<option value="1">12:00 AM</option>
										<option value="1">01:00 AM</option>
										<option value="1">02:00 AM</option>
										<option value="1">03:00 AM</option>
										<option value="1">04:00 AM</option>
										<option value="1">05:00 AM</option>
										<option value="1">06:00 AM</option>
									</select>
								</div>
							</div>							
							<div class="row">
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Select car type</option>
										<option value="1">Micro</option>
										<option value="2">Mini</option>
										<option value="3">Prime</option>
										<option value="1">Prime SUV</option>
										<option value="1">Luxury Cars</option>
										<option value="1">Mini Van</option>
										<option value="1">Small Bus</option>
										<option value="1">Luxury Bus</option>
									</select>
								</div>
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Total passengers</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="1">4</option>
										<option value="1">5</option>
										<option value="1">10</option>
										<option value="1">15</option>
										<option value="1">20</option>
										<option value="1">50</option>
										<option value="1">100</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>No of adults</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="1">4</option>
										<option value="1">5</option>
										<option value="1">6</option>
									</select>
								</div>
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>No of childrens</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="1">4</option>
										<option value="1">5</option>
										<option value="1">6</option>											
									</select>
								</div>
							</div>							

							<div class="row">
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Min Price</option>
										<option value="1">$200</option>
										<option value="2">$500</option>
										<option value="3">$1000</option>
										<option value="1">$5000</option>
										<option value="1">$10,000</option>
										<option value="1">$50,000</option>
									</select>
								</div>
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Max Price</option>
										<option value="1">$200</option>
										<option value="2">$500</option>
										<option value="3">$1000</option>
										<option value="1">$5000</option>
										<option value="1">$10,000</option>
										<option value="1">$50,000</option>
									</select>
								</div>								
							</div>							
							<div class="row">
								<div class="input-field col s12">
									<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn v2-ser-btn">
								</div>
							</div>
						</form>
					</div>						
					</div>
					<div class="col-md-6">
					<div class="v2-ho-se-ri">
						<h1>Car Rentals easy now!</h1>
						<p>Experience the various exciting tour and travel packages and Make hotel reservations, find vacation packages, search cheap hotels and events</p>
						<div class="tourz-hom-ser v2-hom-ser">
							<ul>
								<li>
									<a href="index-2.html" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/2.png" alt=""> Tour</a>
								</li>
								<li>
									<a href="index-1.html" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/31.png" alt=""> Flight</a>
								</li>
								<li>
									<a href="index-4.html" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/30.png" alt=""> Car Rentals</a>
								</li>
								<li>
									<a href="index-3.html" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/1.png" alt=""> Hotel</a>
								</li>								
							</ul>
						</div>
					</div>						
					</div>					
				</div>
			</div>
		</div>
	</section>
	<!--END HEADER SECTION-->