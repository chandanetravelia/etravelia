$(document).ready(function () {
});

$('#login_btn').click(function (event) {
    //validate
    event.preventDefault();

    if ($('#username').val().trim().length < 6) {
        alert('Enter valid username');
        $('#username').focus();
        $('#username').addClass('alert-danger');
        return false;
    }
    if ($('#password').val().trim().length < 6) {
        alert('Enter valid password');
        $('#password').focus();
        $('#password').addClass('alert-danger');
        if ($('#username').hasClass('alert-danger'))
            $('#username').removeClass('alert-danger');
        return false;
    }
//    if ($('#login_btn').hasClass('disabled')) {
//        return false;
//    }
//    $('#login_btn').addClass('disabled');

    $('#loader').fadeIn('slow');

    if ($('#password').hasClass('alert-danger'))
        $('#password').removeClass('alert-danger');
    if ($('#username').hasClass('alert-danger'))
        $('#username').removeClass('alert-danger');
    
    var data = {};
    data.username = $('#username').val().trim();
    data.password = $('#password').val().trim();
    data.sname = 'LoginRegister';
    data.fname = 'clientLogin';
    data.isweb = 1;
    data.tval = 1;
    console.log(data);
    $.ajax({
        url: '../admin/api/index.php',
        data: data,
        type: 'POST',
        success: function (data) {
            console.log("response " + data);

            try {
                data = JSON.parse(atob(data));
            } catch (err) {
                //print error here
//                $('#loader').hide();
                $('#login_btn').removeClass('disabled');
                alert('Failed to Communicate Server');
            }
            if (data.status == 0) {
//                $('#loader').hide();
                handleErrorData(data);
            } else {
                //redirect to the loggedinScreed and clear back history
                $('#loader').hide();
                window.location.href = "dashboard.php";
            }

        },
        error: function (err) {
            $('#loader').hide();
            $('#login_btn').removeClass('disabled');
        }
    }).done(function () {
        //ajax complete
        $('#login_btn').removeClass('disabled');
    }).fail(function () {
        //no internet
        $('#loader').hide();
        $('#login_btn').removeClass('disabled');
    });
    return true;
});
function handleErrorData(data) {
    if (data.is_valid_req == 0) {
        window.location.href = 'https://www.etravelia.com';
    } else {
        alert(data.msg);
        $('#username').val();
        $('#password').val();
    }
}