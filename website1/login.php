<?php 
    include_once'includes/header.php';
?>
<!--DASHBOARD-->
	<section>
		<div class="tr-register">
			<div class="tr-regi-form">
				<h4>Sign In</h4>
				<p>It's free and always will be.</p>
                                <form class="col s12" id="" method="post">
					<div class="row">
						<div class="input-field col s12">
							<input type="text" class="validate" id='username'>
							<label>User Name</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
                                                    <input type="password" class="validate" id='password'>
							<label>Password</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field col s12">
                                                    <input type="submit" value="submit" id="login_btn" class="waves-effect waves-light btn-large full-btn"> </div>
					</div>
				</form>
				<p><a href="forgot-pass.php">forgot password</a> | Are you a new user ? <a href="register.php">Register</a>
				</p>
				<div class="soc-login">
					<h4>Sign in using</h4>
					<ul>
						<li><a href="#"><i class="fa fa-facebook fb1"></i> Facebook</a> </li>
						<li><a href="#"><i class="fa fa-twitter tw1"></i> Twitter</a> </li>
						<li><a href="#"><i class="fa fa-google-plus gp1"></i> Google</a> </li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--END DASHBOARD-->
        <script src="js/jspage/login.js"/>
        <?php 
    include_once'includes/footer.php';
?>

