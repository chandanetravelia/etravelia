<section>
		<div class="tourz-search">
			<div class="container">
				<div class="row">
					<div class="tourz-search-1">
						<h1>Plan Your Travel Now!</h1>
						<p>Experience the various exciting tour and travel packages and Make hotel reservations, find vacation packages, search cheap hotels and events</p>
						<form class="tourz-search-form">
							<div class="input-field">
								<input type="text" id="select-city" class="autocomplete">
								<label for="select-city">Enter city</label>
							</div>
							<div class="input-field">
								<input type="text" id="select-search" class="autocomplete">
								<label for="select-search" class="search-hotel-type">Search over a million hotels, tour and travels, sight seeings and more</label>
							</div>
							<div class="input-field">
								<input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
						</form>
						<div class="tourz-hom-ser">
							<ul>
								<li>
									<a href="index.php?pg=etindex_flightet" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/2.png" alt=""> Tour</a>
								</li>
								<li>
									<a href="index.php?pg=etindex_flightet" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/31.png" alt=""> Flight</a>
								</li>
								<li>
									<a href="index.php?pg=etindex_flightet" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/30.png" alt=""> Car Rentals</a>
								</li>
								<li>
									<a href="index.php?pg=etindex_flightet" class="waves-effect waves-light btn-large tourz-pop-ser-btn"><img src="images/icon/1.png" alt=""> Hotel</a>
								</li>								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--END HEADER SECTION-->