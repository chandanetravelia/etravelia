<?php

include_once '../admin/api/Config/Autoload/autoload.php';
if (!isset($_SESSION))
    session_start();
define("REDIRECT_LOC", "login.php");
if ((!isset($_SESSION) || !isset($_SESSION['admin_id']) || !isset($_SESSION['SESSION_TIMEOUT_AFTER']))) {
    header("Location:login.php");
    exit;
}
if ($_SESSION['SESSION_VALID_TILL'] < time()) {
    echo '<script>confirm("Kindly login again to continue");window.location.href="' . REDIRECT_LOC . '"</script>';
    exit;
} else {
    $_SESSION['SESSION_VALID_TILL'] += $_SESSION['SESSION_TIMEOUT_AFTER'];
}
echo "<script>var aid=" . $_SESSION['admin_id'] . "</script>";
?>