<!DOCTYPE html>
<html lang="en">
<head>
	<title>The Travel - E-Travelia</title>
	<!--== META TAGS ==-->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- FAV ICON -->
	<link rel="shortcut icon" href="images/fav.ico">
	<!-- GOOGLE FONTS -->
	<!--<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:400,500,700" rel="stylesheet">-->
	<!-- FONT-AWESOME ICON CSS -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<!--== ALL CSS FILES ==-->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/materialize.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/mob.css">
	<link rel="stylesheet" href="css/animate.css">
        <script src="js/jquery-3.3.1.js"></script>
        
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--====== MOBILE MENU ==========-->
	<section class="mob-top">
		<div class="mob-menu">
			<div class="mob-head-left"> <img src="images/logo.png" alt=""> </div>
			<div class="mob-head-right"> <a href="#"><i class="fa fa-bars mob-menu-icon" aria-hidden="true"></i></a> <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
				<div class="mob-menu-slide">
					<h4>My Profile</h4>
					<ul class='top-menu'>
						<li><a href="db-my-profile.php"><i class="fa fa-sign-in" aria-hidden="true"></i> My Profile</a>
						</li>
						<li><a href="register.php"><i class="fa fa-address-book-o" aria-hidden="true"></i> Register with us</a>
						</li>
						<li><a href="dashboard.php"><i class="fa fa-bookmark-o" aria-hidden="true"></i> My Bookings</a>
						</li>
						<li><a href="db-travel-booking.php"><i class="fa fa-umbrella" aria-hidden="true"></i> Tour Packages</a>
						</li>
						<li><a href="db-hotel-booking.php"><i class="fa fa-bed" aria-hidden="true"></i> Hotel Bookings</a>
						</li>
						<li><a href="db-refund.php"><i class="fa fa-ban" aria-hidden="true"></i> Cancel Bookings</a>
						</li>
						<li><a href="db-all-payment.php"><i class="fa fa-print" aria-hidden="true"></i> Prient E-Tickets</a>
						</li>
						<li><a href="db-payment.php" class="ho-dr-con-last"><i class="fa fa-align-justify" aria-hidden="true"></i> Make Payment</a>
						</li>
					</ul>
					<h4>Tour Packages</h4>
					<ul class='top-menu'>
						<li><a href="package.php">Family Package</a>
						</li>
						<li><a href="package.php">Honeymoon Package</a>
						</li>
						<li><a href="package.php">Group Package</a>
						</li>
						<li><a href="package.php">WeekEnd Package</a>
						</li>
						<li><a href="package.php">Regular Package</a>
						</li>
					</ul>
					<h4>Hotels</h4>
					<ul class='top-menu'>
						<li><a href="hotels-list.php"><i class="fa fa-sign-in" aria-hidden="true"></i> Hotel Booking</a>
						</li>
						<li><a href="hotels-list.php"><i class="fa fa-address-book-o" aria-hidden="true"></i> All Hotels</a>
						</li>
						<li><a href="hotels-list.php"><i class="fa fa-bookmark-o" aria-hidden="true"></i> Find Hotel</a>
						</li>
					</ul>
					<h4>Seight Seeing</h4>
					<ul class='top-menu'>
						<li><a href="places.php"><i class="fa fa-sign-in" aria-hidden="true"></i> 1.1 Seight Seeing</a>
						</li>
						<li><a href="places-1.php"><i class="fa fa-address-book-o" aria-hidden="true"></i> 1.2 Seight Seeing</a>
						</li>
						<li><a href="places-2.php"><i class="fa fa-bookmark-o" aria-hidden="true"></i> 1.3 Seight Seeing</a>
						</li>
					</ul>
					<h4>All Pages</h4>
					<ul class='top-menu'>
						<li><a href="about.php">About Us</a>
						</li>
						<li><a href="testimonials.php">Testimonials</a>
						</li>
						<li><a href="events.php">Events</a>
						</li>
						<li><a href="index.php?pg=etindex_flightet">Flight</a>
						</li>
						<li><a href="index.php?pg=etindex_flightet">Car</a>
						</li>
						<li><a href="index.php?pg=etbloget">Blog</a>
						</li>
						<li><a href="tips.php">Tips Before Travel</a>
						</li>
						<li><a href="price-list.php">Price List</a>
						</li>
						<li><a href="discount.php">Discount</a>
						</li>
						<li><a href="faq.php">FAQ</a>
						</li>
						<li><a href="sitemap.php">Site map</a>
						</li>
						<li><a href="404.php">404 Page</a>
						</li>
						<li><a href="contact.php">Contact Us</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--====== END MOBILE MENU ==========-->
	<!--====== TOP HEADER ==========-->
	<section>
		<div class="rows head" data-spy="affix" data-offset-top="120">
			<div class="container">
				<div>
					<!--====== BRANDING LOGO ==========-->
					<div class="col-md-4 col-sm-12 col-xs-12 head_left">
                                            <a href="index.php" style="color: #f70601;font-size: 30px;">E<span style="font-size: 22px;color: #fbe655;">Travelia</span></a>
					</div>
					<!--====== HELP LINE & EMAIL ID ==========-->
					<div class="col-md-8 col-sm-12 col-xs-12 head_right head_right_all">
						<ul>
							<li><a href="#">Help Line: +91-7985289314</a> </li>
							<li><a href="#">Email: etraveliainfo@gmail.com</a> </li>
							<li>
								<!--<div class="dropdown">
                                    <button class="dropbtn">My Account</button>
                                    <div class="dropdown-content">
                                        <a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</a>
                                        <a href="#"><i class="fa fa-address-book-o" aria-hidden="true"></i> Register with us</a>
                                        <a href="#"><i class="fa fa-bookmark-o" aria-hidden="true"></i> My Bookings</a>
                                        <a href="#"><i class="fa fa-umbrella" aria-hidden="true"></i> Tour Packages</a>
                                        <a href="#"><i class="fa fa-bed" aria-hidden="true"></i> Hotel Bookings</a>
                                        <a href="#"><i class="fa fa-ban" aria-hidden="true"></i> Cancel Bookings</a>
                                        <a href="#"><i class="fa fa-print" aria-hidden="true"></i> Prient E-Tickets</a>
                                        <a href="#" class="ho-dr-con-last"><i class="fa fa-align-justify" aria-hidden="true"></i> Custom Tour Plan</a>
                                    </div>
								</div>	--><a class='dropdown-button waves-effect waves-light profile-btn' href='#!' data-activates='myacc'><i class="fa fa-user" aria-hidden="true"></i> My Account</a>
								<!-- Dropdown Structure -->
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--====== NAVIGATION MENU ==========-->	
	<section>
		<div class="main-menu-v2">
			<div class="container">
				<div class="row">
					<ul>
						<li><a class='dropdown-button menu-arr' href='#' data-activates='home'>Home</a></li>
						<li><a class='dropdown-button menu-arr' href='#' data-activates='package'>Package</a></li>
						<li><a href="hotels-list.php">Hotels</a></li>
						<li><a href="events.php">Events</a></li>
						<li><a href="index.php?pg=etindex_flightet">Flight</a></li>
						<li><a href="index.php?pg=etindex_caret">Car</a></li>
						<li><a href="index.php?pg=etbloget">Blog</a></li>
						<li><a href="contact.php">Contact Us</a></li>
						<li><a class='dropdown-button menu-arr' href='#' data-activates='allpage'>All Pages</a></li>
						<li id="main-menu-v2-book"><a href="booking.php">Book Your Package</a></li>
					</ul>
				<div>
			<div>
		<div>
		<div class="all-drop-menu">
		  <!-- PACKAGE DROP DOWN MENU -->
		<ul id='myacc' class='dropdown-content v2_head_right'>
			<li><a href="db-my-profile.php"><i class="fa fa-sign-in" aria-hidden="true"></i> My Profile</a>
			</li>
			<li><a href="register.php"><i class="fa fa-address-book-o" aria-hidden="true"></i> Register with us</a>
			</li>
			<li><a href="dashboard.php"><i class="fa fa-bookmark-o" aria-hidden="true"></i> My Bookings</a>
			</li>
			<li><a href="db-travel-booking.php"><i class="fa fa-umbrella" aria-hidden="true"></i> Tour Packages</a>
			</li>
			<li><a href="db-hotel-booking.php"><i class="fa fa-bed" aria-hidden="true"></i> Hotel Bookings</a>
			</li>
			<li><a href="db-refund.php"><i class="fa fa-ban" aria-hidden="true"></i> Cancel Bookings</a>
			</li>
			<li><a href="db-all-payment.php"><i class="fa fa-print" aria-hidden="true"></i> Prient E-Tickets</a>
			</li>
			<li><a href="db-payment.php" class="ho-dr-con-last"><i class="fa fa-align-justify" aria-hidden="true"></i> Make Payment</a>
			</li>
		</ul>		
		  <!-- HOME DROP DOWN MENU -->
		  <ul id='home' class='dropdown-content v2-sub-sinl-men'>
			<li><a href="index.php">Home Page</a></li>
			<li><a href="index.php?pg=etindex_flightet">Home 1 - Flight Booking</a></li>
			<li><a href="index.php?pg=etindex_touret">Home 2 - Tour Booking</a></li>
			<li><a href="index.php?pg=etindex_hotelet">Home 3 - Hotel Booking</a></li>
			<li><a href="index.php?pg=etindex_caret">Home 4 - Car Rentals </a></li>
		  </ul>
		  <!-- PACKAGE DROP DOWN MENU -->
		  <ul id='package' class='dropdown-content v2-sub-sinl-men'>
			<li><a href="package.php">Family Package</a> </li>
			<li><a href="package.php">Honeymoon Package</a> </li>
			<li><a href="package.php">Group Package</a> </li>
			<li><a href="package.php">WeekEnd Package</a> </li>
			<li><a href="package.php">Regular Package</a> </li>
		  </ul>	
		  
			<div id='allpage' class='dropdown-content drop-v2-all'>
				<div class="drop-v2-all-inn">
					<div class="menu-sub-drop">
						<h4>Other Pages</h4>
						<ul>
							<li><a href="about.php"><i class="fa fa-user-o"></i> About Us</a> </li>
							<li><a href="testimonials.php"><i class="fa fa-commenting-o"></i> Testimonials</a> </li>
							<li><a href="events.php"><i class="fa fa-flag-checkered"></i> Events</a> </li>
							<li><a href="tips.php"><i class="fa fa-plane"></i> Tips Before Travel</a> </li>
							<li><a href="price-list.php"><i class="fa fa-usd"></i> Price List</a> </li>
							<li><a href="discount.php"><i class="fa fa-scissors"></i> Discount</a> </li>
							<li><a href="faq.php"><i class="fa fa-question"></i> FAQ</a> </li>
							<li><a href="sitemap.php"><i class="fa fa-sitemap"></i> Site map</a> </li>
							<li><a href="404.php"><i class="fa fa-exclamation-triangle"></i> 404 Page</a> </li>
							<li><a href="login.php"><i class="fa fa-sign-in"></i> Booking</a></li>							
						</ul>
					</div>
					<div class="menu-sub-drop">
						<h4>Dashboard Pages</h4>
						<ul>
						<li><a href="db-my-profile.php"><i class="fa fa-address-book-o"></i> My Profile</a>
						</li>
						<li><a href="dashboard.php"><i class="fa fa-bookmark-o"></i> My Bookings</a>
						</li>
						<li><a href="db-travel-booking.php"><i class="fa fa-umbrella"></i> Tour Packages</a>
						</li>
						<li><a href="db-hotel-booking.php"><i class="fa fa-bed"></i> Hotel Bookings</a>
						</li>
						<li><a href="db-refund.php"><i class="fa fa-ban"></i> Cancel Bookings</a>
						</li>
						<li><a href="db-all-payment.php"><i class="fa fa-print"></i> Prient E-Tickets</a>
						</li>
						<li><a href="db-payment.php" class="ho-dr-con-last"><i class="fa fa-align-justify"></i> Make Payment</a>
						</li>
						<li><a href="register.php"><i class="fa fa-address-book-o"></i> Register with us</a>
						</li>
						<li><a href="login.php"><i class="fa fa-sign-in"></i> Log In</a>
						</li>						
						</ul>
					</div>					
					<div class="menu-sub-drop">
						<h4>Other Pages</h4>
						<ul>
							<li><a href="index.php"><i class="fa fa-check"></i> Home Page</a></li>
							<li><a href="index.php?pg=etindex_flightet"><i class="fa fa-check"></i> Home 1 - Flight Booking</a></li>
							<li><a href="index.php?pg=etindex_touret"><i class="fa fa-check"></i> Home 2 - Tour Booking</a></li>
							<li><a href="index.php?pg=etindex_hotelet"><i class="fa fa-check"></i> Home 3 - Hotel Booking</a></li>
							<li><a href="index.php?pg=etindex_caret"><i class="fa fa-check"></i> Home 4 - Car Rentals </a></li>
							<li><a href="hotels-list.php"><i class="fa fa-check"></i> Hotels</a></li>		
							<li><a href="events.php"><i class="fa fa-check"></i> Events</a></li>
                                                        <li><a href="index.php?pg=etindex_flightet"><i class="fa fa-check"></i>Flight</a></li>
                                                        <li><a href="index.php?pg=etindex_caret"><i class="fa fa-check"></i>Car</a></li>
							<li><a href="contact.php"><i class="fa fa-check"></i> Contact Us</a> </li>
						</ul>						
					</div>
					<div class="menu-sub-drop">
						<h4>Other Pages</h4>
						<ul>
							<li><a href="package.php"><i class="fa fa-check"></i> Family Package</a> </li>
							<li><a href="package.php"><i class="fa fa-check"></i> Honeymoon Package</a> </li>
							<li><a href="package.php"><i class="fa fa-check"></i> Group Package</a> </li>
							<li><a href="package.php"><i class="fa fa-check"></i> WeekEnd Package</a> </li>
							<li><a href="package.php"><i class="fa fa-check"></i> Regular Package</a> </li>
						</ul>
					</div>					
					<div class="menu-sub-drop">
						<h4>Other Pages</h4>
						<ul>
							<li><a href="places.php"><i class="fa fa-check"></i> Sight Seeing - 1</a> </li>
							<li><a href="places-1.php"><i class="fa fa-check"></i> Sight Seeing - 2</a> </li>
							<li><a href="places-2.php"><i class="fa fa-check"></i> Sight Seeing - 3</a> </li>					
						</ul>
					</div>					
				</div>
			</div>
		</div>
	</section>